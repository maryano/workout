package mg.itu.workout;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

public class Login extends AppCompatActivity {

    EditText edit_password;
    private EditText edit_email;
    private FrameLayout btn;
    private ProgressBar progressbar;
    ImageView toggle;
    private FirebaseAuth mAuth;
    private TextView txtSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode(Locale.getDefault().getDisplayLanguage());
        initViews();

        btn.setOnClickListener(view -> loginUserAccount());
        txtSignup.setOnClickListener(v -> startActivity(new Intent(this, RegisterActivity.class)));
    }

    private void initViews() {
        edit_password = findViewById(R.id.edit_password);
        toggle = findViewById(R.id.toggle);
        edit_email = findViewById(R.id.edit_email);
        edit_password = findViewById(R.id.edit_password);
        btn = findViewById(R.id.login_btn);
        progressbar = findViewById(R.id.progressBar);
        txtSignup = findViewById(R.id.txtSignup);
    }

    private void loginUserAccount() {
        progressbar.setVisibility(View.VISIBLE);

        String email, password;
        email = edit_email.getText().toString();
        password = edit_password.getText().toString();

        if(Utils.isNetworkAvailable(this)) {
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
            progressbar.setVisibility(View.GONE);
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), getString(R.string.email_must_be_filled), Toast.LENGTH_SHORT).show();
            progressbar.setVisibility(View.GONE);
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Password must be filled", Toast.LENGTH_SHORT).show();
            progressbar.setVisibility(View.GONE);
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                progressbar.setVisibility(View.GONE);
                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
            } else {
                if(task.getException() != null) {
                    Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    progressbar.setVisibility(View.GONE);
                } else {
                    Toast.makeText(this, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void ShowHidePass(View view) {
        if (view.getId() == R.id.toggle) {
            if (edit_password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                toggle.setImageResource(R.drawable.ic_visibility_off);

                //Show Password
                edit_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.ic_visibility);

                //Hide Password
                edit_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }
}