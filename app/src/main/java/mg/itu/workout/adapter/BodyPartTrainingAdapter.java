package mg.itu.workout.adapter;

import static mg.itu.workout.Training.BODY_PART_KEY;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mg.itu.workout.BodyPartTrainingItem;
import mg.itu.workout.R;
import mg.itu.workout.TrainingsActivity;

public class BodyPartTrainingAdapter extends RecyclerView.Adapter<BodyPartTrainingAdapter.ViewHolder> {

    private ArrayList<BodyPartTrainingItem> items = new ArrayList<>();
    private Context context;

    public BodyPartTrainingAdapter(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setItems(ArrayList<BodyPartTrainingItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.partie_corps_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bodyPartName.setText(items.get(position).getName());
        holder.duration.setText(String.valueOf(items.get(position).getDurationMin()));
        holder.img.setImageResource(items.get(position).getImgResourceId());
        
        holder.parent.setOnClickListener(view -> {
            Intent intent = new Intent(context, TrainingsActivity.class);
            intent.putExtra(BODY_PART_KEY, items.get(position));
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView bodyPartName, duration;
        private ImageView img;
        private RelativeLayout parent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bodyPartName = itemView.findViewById(R.id.bodyPartName);
            duration = itemView.findViewById(R.id.duration);
            img = itemView.findViewById(R.id.img);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
