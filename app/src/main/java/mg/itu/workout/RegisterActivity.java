package mg.itu.workout;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {
    private EditText emailTextView, passwordTextView, displayNameEdtTxt;
    private Button btnSignup;
    private ProgressBar progressbar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode(Locale.getDefault().getDisplayLanguage());

        initViews();

        btnSignup.setOnClickListener(v -> registerNewUser());
    }

    private void initViews() {
        emailTextView = findViewById(R.id.email);
        passwordTextView = findViewById(R.id.passwd);
        btnSignup = findViewById(R.id.btnregister);
        progressbar = findViewById(R.id.progressbar);
        displayNameEdtTxt = findViewById(R.id.displayNameEdtTxt);
    }

    private void registerNewUser() {
        progressbar.setVisibility(View.VISIBLE);

        String email, password, displayName;
        email = emailTextView.getText().toString();
        password = passwordTextView.getText().toString();
        displayName = displayNameEdtTxt.getText().toString();

        if(Utils.isNetworkAvailable(this)) {
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
            progressbar.setVisibility(View.GONE);
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), getString(R.string.email_must_be_filled), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), getString(R.string.email_must_be_filled), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), getString(R.string.pwd_must_be_filled), Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null) {
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(displayName).build();
                    user.updateProfile(profileUpdates).addOnCompleteListener(task1 -> {
                        progressbar.setVisibility(View.GONE);
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                    }).addOnFailureListener(e -> {
                        Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        progressbar.setVisibility(View.GONE);
                    });
                }
            } else {
                if(task.getException() != null) {
                    Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    progressbar.setVisibility(View.GONE);
                } else {
                    Toast.makeText(this, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}