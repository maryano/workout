package mg.itu.workout.modal;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.concurrent.TimeUnit;

import mg.itu.workout.ExecutedTrainingActivity;
import mg.itu.workout.R;
import mg.itu.workout.TimeWatch;
import mg.itu.workout.Utils;

public class PauseDialog extends DialogFragment {

    private TextView timer;
    Thread timerThread;
    private int pauseTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pause_dialog, container, false);
        pauseTime = Utils.getPauseTimePreference(getActivity());
        initViews(v);
        initTimerThread();
        timerThread.start();
        return v;
    }

    /*@NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.pause_dialog, null);
        initViews(view);
        initTimerThread();
        timerThread.start();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(view);
        Dialog result = builder.create();
        return result;
    }*/

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        try {
            timerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            timerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initViews(View view) {
        timer = view.findViewById(R.id.timer);
    }

    private void initTimerThread() {
        TimeWatch watch = TimeWatch.start();
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            private long startTime = System.currentTimeMillis();

            @SuppressLint({"SetTextI18n", "DefaultLocale"})
            public void run() {
                while (pauseTime > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(() -> {
                        pauseTime--;
                        timer.setText("00:" + String.format("%02d", pauseTime));
                    });
                }
                dismiss();
            }
        };

        timerThread = new Thread(runnable);
    }
}
