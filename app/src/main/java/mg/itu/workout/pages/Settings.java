package mg.itu.workout.pages;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import mg.itu.workout.R;
import mg.itu.workout.Utils;
import mg.itu.workout.fragment.TimePreferenceDialogFragmentCompat;
import mg.itu.workout.notification.NotifierAlarm;
import mg.itu.workout.preference.TimePreference;

public class Settings extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private Preference pref;
    int prefixStr;
    AlarmManager alarmMgr;
    PendingIntent alarmIntent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.root_preferences);

        Intent intent = new Intent(getActivity(), NotifierAlarm.class);
        alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
        alarmMgr = (AlarmManager) getActivity().getSystemService(getActivity().ALARM_SERVICE);

        SharedPreferences sharedPref = getPreferenceScreen().getSharedPreferences();
        sharedPref.registerOnSharedPreferenceChangeListener(this);

        pref = findPreference("mensa_closing_time");

        //Get the user input data
        prefixStr = sharedPref.getInt("mensa_closing_time", -1);
        @SuppressLint("DefaultLocale") String result = String.format("%02d", prefixStr/60) + ":"+ String.format("%02d", prefixStr%60);

        //Update the summary with user input data
        pref.setSummary(result);


    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        if (preference instanceof TimePreference) {
            DialogFragment dialogFragment = TimePreferenceDialogFragmentCompat.newInstance(preference.getKey());
            dialogFragment.setTargetFragment(this, 0);
            assert getFragmentManager() != null;
            dialogFragment.show(getFragmentManager(), null);
        }
        else {
            super.onDisplayPreferenceDialog(preference);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if(s.equals("mensa_closing_time_active")) {
            pref = findPreference(s);
            boolean enabled = sharedPreferences.getBoolean(s, false);
            if(enabled) {
                setReminder(sharedPreferences);
            } else {
                Utils.stopReminder(alarmMgr, alarmIntent, getActivity());
            }
        }


        if(s.equals("mensa_closing_time")) {
            setReminder(sharedPreferences);
            //Get the current summary
        }
    }

    private void setReminder(SharedPreferences sharedPreferences) {
        pref = findPreference("mensa_closing_time");

        //Get the user input data
        prefixStr = sharedPreferences.getInt("mensa_closing_time", -1);
        @SuppressLint("DefaultLocale") String result = String.format("%02d", prefixStr/60) + ":"+ String.format("%02d", prefixStr%60);

        //Update the summary with user input data
        pref.setSummary(result);
        Utils.setReminder(prefixStr/60, prefixStr%60, alarmMgr, alarmIntent,getActivity());

    }
}