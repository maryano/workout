package mg.itu.workout.adapter;

import static mg.itu.workout.Training.TRAINING_KEY;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mg.itu.workout.R;
import mg.itu.workout.Training;
import mg.itu.workout.modal.DetailExerciceModal;

public class TrainingAdapterSearch extends RecyclerView.Adapter<TrainingAdapterSearch.ViewHolder> {

    private ArrayList<Training> trainings = new ArrayList<>();
    private Context context;

    public TrainingAdapterSearch(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setTrainings(ArrayList<Training> trainings) {
        this.trainings = trainings;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.training_item_search, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(trainings.get(position).getName());
        holder.duration.setText(context.getString(R.string._00)+ " " + trainings.get(position).getDuration());
        holder.img.setImageResource(trainings.get(position).getImgResId());
        holder.parent.setOnClickListener(view -> {
            DetailExerciceModal modal = new DetailExerciceModal();
            Bundle bundle = new Bundle();
            bundle.putParcelable(TRAINING_KEY, trainings.get(position));
            modal.setArguments(bundle);
            modal.show(((FragmentActivity)context).getSupportFragmentManager(), "detail_exercice_modal");
        });
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name, duration;
        private ImageView img;
        private ConstraintLayout parent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            duration = itemView.findViewById(R.id.duration);
            img = itemView.findViewById(R.id.img);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
