package mg.itu.workout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("CustomSplashScreen")
public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utils.initSharedPreferences(this);
        if(Utils.loggedInCheck(this) == 0)
            startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
