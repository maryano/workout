package mg.itu.workout.fragment;

import static mg.itu.workout.Training.BODY_PART_KEY;
import static mg.itu.workout.Training.TRAINING_KEY;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mg.itu.workout.BodyPartTrainingItem;
import mg.itu.workout.R;
import mg.itu.workout.TrainingsActivity;

public class BodyPartTrainingFragment extends Fragment {
    private TextView bodyPartName, duration;
    private ImageView img;
    private RelativeLayout parent;
    private View separator;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.partie_corps_item, container, false);
        initViews(view);
        Bundle bundle = getArguments();
        if(bundle != null) {
            BodyPartTrainingItem bodyPartTrainingItem = bundle.getParcelable(BODY_PART_KEY);
            if(bodyPartTrainingItem != null) {
                bodyPartName.setText(bodyPartTrainingItem.getName());
                duration.setText(String.valueOf(bodyPartTrainingItem.getDurationMin()));
                img.setImageResource(bodyPartTrainingItem.getImgResourceId());

                parent.setOnClickListener(v -> {
                    Intent intent = new Intent(getActivity(), TrainingsActivity.class);
                    intent.putExtra(BODY_PART_KEY, bodyPartTrainingItem);
                    startActivity(intent);
                });

            }

        }
        return view;
    }

    private void initViews(View view) {
        bodyPartName = view.findViewById(R.id.bodyPartName);
        duration = view.findViewById(R.id.duration);
        img = view.findViewById(R.id.img);
        parent = view.findViewById(R.id.parent);
        separator = view.findViewById(R.id.separator);

        separator.setVisibility(View.GONE);
    }
}
