package mg.itu.workout;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import mg.itu.workout.notification.NotifierAlarm;

public class Utils {

    private static final String TAG = "Utils";

    public static final String INTENT_MESSAGE_KEY = "intent_message";
    public static final String MOST_RECENT_TRAINING_KEY = "most_recent_training";
    public static final String PAUSE_TIME_KEY = "pause_time";
    public static final String ALL_TRAININGS_KEY = "all_trainings";
    public static final String DB_NAME = "fake_database";

    private static final Gson gson = new Gson();
    private static final Type trainingType = new TypeToken<ArrayList<Training>>() {
    }.getType();

    public static int loggedInCheck(Context context) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(context, Login.class);
            intent.putExtra(INTENT_MESSAGE_KEY, context.getString(R.string.reconnect));
            context.startActivity(intent);
            return 1;
        }
        return 0;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null || !activeNetworkInfo.isConnected();
    }

    public static BodyPartTrainingItem getMostRecentlyBodyPartTraining(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        Type trainingType = new TypeToken<BodyPartTrainingItem>() {
        }.getType();
        return gson.fromJson(sharedPreferences.getString(MOST_RECENT_TRAINING_KEY, null), trainingType);
    }

    @SuppressLint("ApplySharedPref")
    public static void updateMostRecentlyBodyPartTraining(Context context, BodyPartTrainingItem bodyPartTrainingItem) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(MOST_RECENT_TRAINING_KEY);
        editor.putString(MOST_RECENT_TRAINING_KEY, gson.toJson(bodyPartTrainingItem));
        editor.commit();
    }

    public static int extractIntFromString(String string) {
        return Integer.parseInt(string.replaceAll("[^0-9]", ""));
    }

    public static int[] getTrainingReminderTime(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int value = sharedPreferences.getInt("mensa_closing_time", -1);
        if (value != -1) {
            return new int[]{value / 60, value % 60};
        }
        return null;
    }

    public static int getPauseTimePreference(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = sharedPreferences.getString(PAUSE_TIME_KEY, null);
        if (value != null) {
            return extractIntFromString(value);
        }
        return -1;
    }

    public static void initSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        ArrayList<BodyPartTrainingItem> allTrainings = gson.fromJson(sharedPreferences.getString(ALL_TRAININGS_KEY, null), trainingType);
        if (allTrainings == null) {
            initAllItems(context);
        }
    }

    @SuppressLint("ApplySharedPref")
    private static void initAllItems(Context context) {
        ArrayList<Training> allTrainings = new ArrayList<>();

        // ARMS
        Training pushUps = new Training("Push-ups", 10, "Raising and lowering your body with your arms, keeping your body upright, this exercise works the muscles of the chest, shoulders, triceps, back and legs", Training.ARMS_BODY_PART, R.drawable.pushups);
        allTrainings.add(pushUps);

        Training punches = new Training("Punches", 10, "Stand with one leg forward and knees slightly bent. Bend your elbows, clench your fists and place them in front of your face. Bring the fist back, extend the other arm and repeat.", Training.ARMS_BODY_PART, R.drawable.punches);
        allTrainings.add(punches);

        Training triceps = new Training("Triceps", 10, "Hold the edges of a raised surface while you lower your hips. While keeping your back close to the platform, bend your elbows and try to get down to 90 degrees. This is a great exercise for the triceps.", Training.ARMS_BODY_PART, R.drawable.triceps);
        allTrainings.add(triceps);

        // CHEST
        Training chestPressPunchUp = new Training("Chest Press Punch Up", 15, "1. Lie on your back with your knees bent, your back flat, and hold a dumbbell in each hand.\n" +
                "2. Squeeze your abs, lift your right shoulder off the floor and punch up with your right arm.\n" +
                "3. Lower your arm to the starting position and repeat with the left arm.", Training.CHEST_BODY_PART, R.drawable.chest_press_punch_up);
        allTrainings.add(chestPressPunchUp);

        Training crossJacks = new Training("Cross jacks", 15, "1. Stand with your feet shoulder-width apart, hold a dumbbell in each hand, and raise the dumbbells until your upper arms are parallel to the floor and your elbows are directly under the wrists.\n" +
                "2. Bring the elbows and forearms toward the midline of the body and then return to the starting position.\n" +
                "3. Push the dumbbells up and fully extend your arms.\n" +
                "4. Return to the starting position and repeat the movement until the set is complete." +
                "3. Lower your arm to the starting position and repeat with the left arm.", Training.CHEST_BODY_PART, R.drawable.cross_jacks);
        allTrainings.add(crossJacks);

        Training elbow_squeeze_shoulder_press = new Training("Elbow Squeeze Shoulder", 15, "1. Stand straight with your feet shoulder-width apart and your arms up and extended out to the sides.\n" +
                "2. Jump up and cross your left leg in front of the right, and your left arm on top of the right.\n" +
                "3. Jump again and return to the starting position.\n" +
                "4. Repeat, and reverse the position of your arms and legs.", Training.CHEST_BODY_PART, R.drawable.elbow_squeeze_shoulder_press);
        allTrainings.add(elbow_squeeze_shoulder_press);

        // CRUNCHES
        Training climb = new Training("Climb", 15, "Start in a pump position, bend the right knee toward the chest, jump up and switch legs in the air, bending the left foot and stretching the right leg. \n\nThis strengthens several muscle groups in addition to the cardio system." +
                "4. Repeat, and reverse the position of your arms and legs.", Training.CRUNCHES_BODY_PART, R.drawable.climb);
        allTrainings.add(climb);

        Training legLift = new Training("Leg Lift", 15, "Lie on your back and place your hands under your hips for support. \\n\\n Raise your outstretched legs so that they are at right angles to the floor. Slowly lower your legs and repeat the exercise.", Training.CRUNCHES_BODY_PART, R.drawable.leg_lift);
        allTrainings.add(legLift);

        Training abdominalCrunch = new Training("Abdominal crunch", 15, "Lie on your back with your knees bent and your arms extended forward. \\n\\nThen lift your upper body. Hold for a few seconds and slowly lower back down. \\n\\nThis works primarily the rectus abdominis and obliques.", Training.CRUNCHES_BODY_PART, R.drawable.abdominal_crunch);
        allTrainings.add(abdominalCrunch);

        // LEG
        Training kneeStretch = new Training("Knee Stretch", 15, "Lie on the floor with your legs straight. Raise your left knee and grab it with both hands. \\n\\nPull your left knee toward your chest as much as possible while keeping your right leg straight on the floor. \\n\\nHold this position for a few seconds and repeat the exercise with the other knee.", Training.LEGS_BODY_PART, R.drawable.knee_stretch);
        allTrainings.add(kneeStretch);

        Training squats = new Training("Squats", 15, "Stand with your feet shoulder-width apart and your arms extended forward, then lower your body until your thighs are parallel to the floor. \\n\\nYour knees should bend in the same direction as your toes? Return to the starting position and proceed to the next repetition.", Training.LEGS_BODY_PART, R.drawable.squats);
        allTrainings.add(squats);

        Training sideJump = new Training("Side jump", 15, "Stand, put your hands in front of you and jump from side to side.", Training.LEGS_BODY_PART, R.drawable.side_jump);
        allTrainings.add(sideJump);


        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ALL_TRAININGS_KEY, gson.toJson(allTrainings));
        editor.commit();
    }

    public static ArrayList<Training> getAllTrainings(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        return gson.fromJson(sharedPreferences.getString(ALL_TRAININGS_KEY, null), trainingType);
    }

    public static ArrayList<BodyPartTrainingItem> getBodyPartTrainings(Context context) {
        ArrayList<Training> allTrainings = getAllTrainings(context);
        if (allTrainings != null) {
            BodyPartTrainingItem arms = new BodyPartTrainingItem(context.getString(R.string.arms), R.drawable.arms, Training.ARMS_BODY_PART);
            BodyPartTrainingItem legs = new BodyPartTrainingItem(context.getString(R.string.legs), R.drawable.legs, Training.LEGS_BODY_PART);
            BodyPartTrainingItem crunches = new BodyPartTrainingItem(context.getString(R.string.crunches), R.drawable.crunches, Training.CRUNCHES_BODY_PART);
            BodyPartTrainingItem chest = new BodyPartTrainingItem(context.getString(R.string.chest), R.drawable.chest, Training.CHEST_BODY_PART);

            ArrayList<BodyPartTrainingItem> bodyPartsTrainings = new ArrayList<>();
            bodyPartsTrainings.add(arms);
            bodyPartsTrainings.add(legs);
            bodyPartsTrainings.add(crunches);
            bodyPartsTrainings.add(chest);

            for (Training training : allTrainings) {
                if (training.getBodyPartId() == Training.ARMS_BODY_PART) {
                    arms.setDuration(arms.getDuration() + training.getDuration());
                } else if (training.getBodyPartId() == Training.LEGS_BODY_PART) {
                    legs.setDuration(legs.getDuration() + training.getDuration());
                } else if (training.getBodyPartId() == Training.CRUNCHES_BODY_PART) {
                    crunches.setDuration(crunches.getDuration() + training.getDuration());
                } else if (training.getBodyPartId() == Training.CHEST_BODY_PART) {
                    chest.setDuration(chest.getDuration() + training.getDuration());
                } else {
                    throw new IllegalStateException("Unknown Body Part");
                }
            }

            return bodyPartsTrainings;
        }
        return null;
    }

    public static ArrayList<Training> getTrainingsByBodyPart(int bodyPartId, Context context) {
        ArrayList<Training> allTrainings = getAllTrainings(context);
        if (allTrainings != null) {
            ArrayList<Training> trainings = new ArrayList<>();
            for (Training training : allTrainings) {
                if (training.getBodyPartId() == bodyPartId) {
                    trainings.add(training);
                }
            }
            return trainings;
        }
        return null;
    }

    public static ArrayList<Training> searchTraining(String key, Context context) {
        ArrayList<Training> allTrainings = getAllTrainings(context);
        if (allTrainings != null) {
            ArrayList<Training> trainings = new ArrayList<>();
            for (Training training : allTrainings) {
                if (training.getName().toLowerCase().contains(key.toLowerCase())) {
                    trainings.add(training);
                }

            }
            return trainings;
        }
//        tableau vide
        return new ArrayList<>();
    }

    public static int getPauseTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PAUSE_TIME_KEY, -1);
    }

    public static void setReminder(int hour, int minute, AlarmManager alarmMgr, PendingIntent alarmIntent,Context context) {
        Log.d(TAG, "setReminder: ");

//        setTime
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
//        enable notification at the specifed time, everyday
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
        Toast.makeText(context, context.getString(R.string.reminder_set), Toast.LENGTH_SHORT).show();
    }

    public static void stopReminder(AlarmManager alarmMgr, PendingIntent alarmIntent, Context context) {
        if(alarmMgr != null && alarmIntent != null) {
            alarmMgr.cancel(alarmIntent);
            Toast.makeText(context, context.getString(R.string.reminder_stopped), Toast.LENGTH_SHORT).show();
        }
    }
}
