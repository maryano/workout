package mg.itu.workout.modal;

import static mg.itu.workout.Training.BODY_PART_KEY;
import static mg.itu.workout.Training.TRAINING_KEY;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import mg.itu.workout.BodyPartTrainingItem;
import mg.itu.workout.MainActivity;
import mg.itu.workout.R;
import mg.itu.workout.Utils;

public class CongratsDialog extends DialogFragment {

    Thread timerThread;
    private int dismissTime = 5;
    BodyPartTrainingItem bodyPartTrainingItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.congrats_dialog, container, false);
        initTimerThread();
        timerThread.start();
        Bundle bundle = getArguments();
        if(bundle != null) {
            bodyPartTrainingItem = bundle.getParcelable(BODY_PART_KEY);
        }
        return v;
    }

    private void initTimerThread() {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            private long startTime = System.currentTimeMillis();

            public void run() {
                while (dismissTime > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(() -> dismissTime--);
                }

                if(getActivity() != null) {
                    Utils.updateMostRecentlyBodyPartTraining(getActivity(), bodyPartTrainingItem);
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        };

        timerThread = new Thread(runnable);
    }
}
