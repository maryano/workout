package mg.itu.workout.pages;

import static mg.itu.workout.Training.BODY_PART_KEY;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mg.itu.workout.BodyPartTrainingItem;
import mg.itu.workout.R;
import mg.itu.workout.Utils;
import mg.itu.workout.adapter.BodyPartTrainingAdapter;
import mg.itu.workout.fragment.BodyPartTrainingFragment;

public class Home extends Fragment {

    private RecyclerView trainingRecView;
    private TextView bodyPartCount, noRecentTrainingTxt, pseudo;
    private FrameLayout mostRecentContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(view);

        if(getActivity() != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user != null) {
                pseudo.setText(user.getDisplayName());
                pseudo.setVisibility(View.VISIBLE);
            }

            BodyPartTrainingItem mostRecentBodyPartTraining = Utils.getMostRecentlyBodyPartTraining(getActivity());
            if (mostRecentBodyPartTraining != null) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                BodyPartTrainingFragment mostRecentBodyPartTrainingFragment = new BodyPartTrainingFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(BODY_PART_KEY, mostRecentBodyPartTraining);
                mostRecentBodyPartTrainingFragment.setArguments(bundle);
                mostRecentContainer.setVisibility(View.VISIBLE);
                noRecentTrainingTxt.setVisibility(View.GONE);
                transaction.replace(R.id.mostRecentContainer, mostRecentBodyPartTrainingFragment);
                transaction.commit();
            } else {
                mostRecentContainer.setVisibility(View.GONE);
                noRecentTrainingTxt.setVisibility(View.VISIBLE);
            }

            BodyPartTrainingAdapter adapter = new BodyPartTrainingAdapter(getActivity());
            trainingRecView.setAdapter(adapter);
            trainingRecView.setLayoutManager(new LinearLayoutManager(getActivity()));
            ArrayList<BodyPartTrainingItem> bodyPartTrainings = Utils.getBodyPartTrainings(getActivity());
            if (bodyPartTrainings != null) {
                adapter.setItems(bodyPartTrainings);
                bodyPartCount.setText(String.valueOf(bodyPartTrainings.size()));
            }
        }
        return view;
    }

    private void initViews(View view) {
        trainingRecView = view.findViewById(R.id.trainingRecView);
        bodyPartCount = view.findViewById(R.id.bodyPartCount);
        mostRecentContainer = view.findViewById(R.id.mostRecentContainer);
        noRecentTrainingTxt = view.findViewById(R.id.noRecentTrainingTxt);
        pseudo = view.findViewById(R.id.pseudo);
    }
}