package mg.itu.workout;

import android.os.Parcel;
import android.os.Parcelable;

public class Training implements Parcelable {

    public static final String BODY_PART_KEY = "body_part";
    public static final String TRAINING_KEY = "training";

    public static final int CRUNCHES_BODY_PART = 1;
    public static final int ARMS_BODY_PART = 2;
    public static final int LEGS_BODY_PART = 3;
    public static final int CHEST_BODY_PART = 4;

    private String name;
    private int duration;
    private String description;
    private int bodyPartId;
    private int imgResId;

    public Training(String name, int duration, String description, int bodyPartId, int imgResId) {
        this.name = name;
        this.duration = duration;
        this.description = description;
        this.bodyPartId = bodyPartId;
        this.imgResId = imgResId;
    }

    protected Training(Parcel in) {
        name = in.readString();
        duration = in.readInt();
        description = in.readString();
        bodyPartId = in.readInt();
        imgResId = in.readInt();
    }

    public static final Creator<Training> CREATOR = new Creator<Training>() {
        @Override
        public Training createFromParcel(Parcel in) {
            return new Training(in);
        }

        @Override
        public Training[] newArray(int size) {
            return new Training[size];
        }
    };

    public int getBodyPartId() {
        return bodyPartId;
    }

    public int getDuration() {
        return duration;
    }

    public String getName() {
        return name;
    }

    public int getImgResId() {
        return imgResId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(duration);
        parcel.writeString(description);
        parcel.writeInt(bodyPartId);
        parcel.writeInt(imgResId);
    }

    public String getDescription() {
        return description;
    }
}
