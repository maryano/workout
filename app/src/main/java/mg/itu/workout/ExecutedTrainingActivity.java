package mg.itu.workout;

import static mg.itu.workout.Training.BODY_PART_KEY;
import static mg.itu.workout.TrainingsActivity.TRAININGS_KEY;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import mg.itu.workout.modal.CongratsDialog;
import mg.itu.workout.modal.PauseDialog;

public class ExecutedTrainingActivity extends AppCompatActivity {

    private TextView statusExercises, totalTimeSpent, trainingName, nextTraining, trainingDuration;
    private ImageView imgStartNow, imgQuit, imgTraining;
    Thread timerThread;

    private int indexCurrentTraining;
    private boolean trainingFinished;
    private int curTrainingDuration;
    private int pauseDuration;
    private ArrayList<Training> trainings;
    private Training curTraining;
    private boolean inPauseMode;
    private BodyPartTrainingItem bodyPartTrainingItem;

    @Override
    public void onBackPressed() {
        trainingFinished = true;
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executed_training);

        initViews();
        imgStartNow.setOnClickListener(view -> {
            // TODO: 19/09/2021 start immediat 
        });
        imgQuit.setOnClickListener(view -> {
            // TODO: 19/09/2021 Pause
        });

        Intent intent = getIntent();
        if (intent != null) {
            trainings = intent.getParcelableArrayListExtra(TRAININGS_KEY);
            bodyPartTrainingItem = intent.getParcelableExtra(BODY_PART_KEY);
            if (trainings != null && bodyPartTrainingItem != null) {
                indexCurrentTraining = 0;
                pauseDuration = Utils.getPauseTimePreference(this);
                if (pauseDuration == -1) {
                    throw new IllegalStateException("Pause duration value not allowed");
                }
                updateCurTraining();
                updateNextTrainingName();
                updateUI();
                initTimerThread();
                timerThread.start();

            }
        }
    }

    private void updateCurTraining() {
        curTraining = trainings.get(indexCurrentTraining);
        curTrainingDuration = curTraining.getDuration();
    }

    @SuppressLint("SetTextI18n")
    private void updateUI() {
        statusExercises.setText(getString(R.string.exercises) + " " + (indexCurrentTraining + 1) + "/" + trainings.size());
        Glide.with(this).load(curTraining.getImgResId()).into(imgTraining);
        trainingName.setText(curTraining.getName());
    }

    private void updateTrainingDuration() {
        if (inPauseMode) {
            trainingDuration.setText(String.valueOf(pauseDuration));
        } else {
            trainingDuration.setText(String.valueOf(curTrainingDuration));
        }
    }

    private void updateNextTrainingName() {
        nextTraining.setText(trainings.get(indexCurrentTraining + 1).getName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timerThread != null && timerThread.isAlive()) {
            try {
                timerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void initViews() {
        statusExercises = findViewById(R.id.statusExercises);
        totalTimeSpent = findViewById(R.id.totalTimeSpent);
        trainingName = findViewById(R.id.trainingName);
        nextTraining = findViewById(R.id.nextTraining);
        imgStartNow = findViewById(R.id.imgStartNow);
        imgQuit = findViewById(R.id.imgQuit);
        imgTraining = findViewById(R.id.imgTraining);
        trainingDuration = findViewById(R.id.trainingDuration);
    }

    private void initTimerThread() {
        TimeWatch watch = TimeWatch.start();
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            private long startTime = System.currentTimeMillis();

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void run() {
                while (!trainingFinished) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(() -> {
                        totalTimeSpent.setText(String.format("%02d", watch.time(TimeUnit.MINUTES) % 60) + ":" + String.format("%02d", watch.time(TimeUnit.SECONDS) % 60));

                        if (inPauseMode) {
                            pauseDuration--;
                            if (pauseDuration <= 0) {
                                inPauseMode = false;
                                pauseDuration = Utils.getPauseTimePreference(ExecutedTrainingActivity.this);
                                if (pauseDuration == -1) {
                                    throw new IllegalStateException("Pause duration value not allowed");
                                }
                                trainingDuration.setBackground(AppCompatResources.getDrawable(ExecutedTrainingActivity.this, R.drawable.circle_bg));
                            }
                        } else {
                            curTrainingDuration--;
                            if (curTrainingDuration <= 0) {
                                indexCurrentTraining++;

                                if (indexCurrentTraining >= trainings.size()) {
                                    trainingFinished = true;
                                    return;
                                }

                                inPauseMode = true;
                                trainingDuration.setBackground(AppCompatResources.getDrawable(ExecutedTrainingActivity.this, R.drawable.circle_pause_bg));

//                                    PauseDialog pauseDialog = new PauseDialog();
//                                    pauseDialog.show(getSupportFragmentManager(), "open_pause_dialog");

                                DialogFragment newFragment = new PauseDialog();
                                newFragment.show(getSupportFragmentManager(), "pause_dialog_dbg");

                                updateCurTraining();
                                updateUI();
                                if (indexCurrentTraining == trainings.size() - 1) {
                                    nextTraining.setVisibility(View.GONE);
                                } else {
                                    updateNextTrainingName();
                                }
                            }
                        }
                        updateTrainingDuration();
                    });
                }

                DialogFragment newFragment = new CongratsDialog();
                Bundle bundle = new Bundle();
                bundle.putParcelable(BODY_PART_KEY, bodyPartTrainingItem);
                newFragment.setArguments(bundle);
                newFragment.show(getSupportFragmentManager(), "congrats_dialog_dbg");
            }
        };
        timerThread = new Thread(runnable);
    }
}
