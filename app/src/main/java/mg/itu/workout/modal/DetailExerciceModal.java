package mg.itu.workout.modal;

import static mg.itu.workout.Training.TRAINING_KEY;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;

import mg.itu.workout.R;
import mg.itu.workout.Training;

public class DetailExerciceModal extends DialogFragment {

    private Button closeBtn;
    private ImageView myImg;
    private TextView exerciseTitle, duration, description;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.detail_exercice,null);
        initViews(view);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        Dialog result = builder.create();
        result.setCanceledOnTouchOutside(true);
        result.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Bundle bundle = getArguments();
        if(bundle != null) {
            Training training = bundle.getParcelable(TRAINING_KEY);
            if(training != null) {
                Glide.with(this).load(training.getImgResId()).into(myImg);
                exerciseTitle.setText(training.getName());
                duration.setText(training.getDuration() + " " + getString(R.string.seconds));
                description.setText(training.getDescription());
            }
        }
        return result;
    }

    private void initViews(View view) {
        closeBtn = view.findViewById(R.id.closeExerciceModal);
        myImg = view.findViewById(R.id.myimg);
        exerciseTitle = view.findViewById(R.id.exerice_title);
        duration = view.findViewById(R.id.duration);
        description = view.findViewById(R.id.description);

        closeBtn.setOnClickListener(view1 -> dismiss());
    }
}
