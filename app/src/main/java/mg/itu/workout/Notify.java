package mg.itu.workout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class Notify extends AppCompatActivity {
    NotificationManager manager;
    PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        Glide.with(this).load(R.drawable.pompes).into(imageView);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        Intent intent = new Intent(this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        try{
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("my Notification","my Notification", NotificationManager.IMPORTANCE_DEFAULT);
            manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        }catch(Exception e){
            Toast t = Toast.makeText(Notify.this,e.getMessage(),Toast.LENGTH_SHORT);
            t.show();
        }
    }
    public void notifyMe(View view){
        try{
        NotificationCompat.Builder builder = new NotificationCompat.Builder(Notify.this, "my Notification");
        builder.setContentTitle("gentil rappel");
        builder.setContentText("n'oubliez pas votre entrainement prévu pour 14:00!");
        builder.setSmallIcon(R.drawable.logo_gris);
        builder.setContentIntent(pendingIntent);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText("Much longer text that cannot fit one line..."));
        builder.setAutoCancel(true);
        builder.setCategory(NotificationCompat.CATEGORY_REMINDER);
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(Notify.this);
        managerCompat.notify(1, builder.build());
        }catch(Exception e){
            Toast t = Toast.makeText(Notify.this,e.getMessage(),Toast.LENGTH_SHORT);
            t.show();
        }
    }
}