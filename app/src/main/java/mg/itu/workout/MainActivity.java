package mg.itu.workout;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import mg.itu.workout.pages.Home;
import mg.itu.workout.pages.Search;
import mg.itu.workout.pages.Settings;

public class MainActivity extends AppCompatActivity {

    final Fragment fragment1 = new Home();
    final Fragment fragment2 = new Search();
    final Fragment fragment3 = new Settings();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Utils.loggedInCheck(this);

        fm.beginTransaction().add(R.id.main_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.main_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.main_container, fragment1, "1").commit();
        ((BottomNavigationView) findViewById(R.id.navigation)).setOnItemSelectedListener(itemSelectedListener);
    }

    @SuppressLint("NonConstantResourceId")
    NavigationBarView.OnItemSelectedListener itemSelectedListener = item -> {
        switch (item.getItemId()) {
            case R.id.nav_home:
                fm.beginTransaction().hide(active).show(fragment1).commit();
                active = fragment1;
                return true;

            case R.id.nav_search:
                fm.beginTransaction().hide(active).show(fragment2).commit();
                active = fragment2;
                return true;
            case R.id.nav_settings:
                fm.beginTransaction().hide(active).show(fragment3).commit();
                active = fragment3;
                return true;
        }
        return false;
    };


}