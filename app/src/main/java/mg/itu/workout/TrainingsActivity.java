package mg.itu.workout;

import static mg.itu.workout.Training.BODY_PART_KEY;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import mg.itu.workout.adapter.TrainingAdapter;

public class TrainingsActivity extends AppCompatActivity {

    public static final String TRAININGS_KEY = "trainings";

    private RecyclerView trainingsRecView;
    private TextView title, duration, bodyPartName, nbTrainings;
    private Button btnStart;
    
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainings);
        initViews();
        Intent intent = getIntent();
        if(intent != null) {
            BodyPartTrainingItem bodyPartTraining = intent.getParcelableExtra(BODY_PART_KEY);
            if(bodyPartTraining != null) {
                TrainingAdapter adapter = new TrainingAdapter(this);
                trainingsRecView.setAdapter(adapter);
                trainingsRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                ArrayList<Training> trainings = Utils.getTrainingsByBodyPart(bodyPartTraining.getBodyPartId(), this);
                if(trainings != null) {
                    adapter.setTrainings(trainings);

                    title.setText(getString(R.string.training) + " " + bodyPartTraining.getName());
                    duration.setText(bodyPartTraining.getDurationMin() + " " + getString(R.string.min));
                    bodyPartName.setText(bodyPartTraining.getName());
                    nbTrainings.setText(new StringBuilder().append("(").append(trainings.size()).append(")"));

                    btnStart.setOnClickListener(view -> {
                        Intent i = new Intent(this, ExecutedTrainingActivity.class);
                        i.putParcelableArrayListExtra(TRAININGS_KEY, trainings);
                        i.putExtra(BODY_PART_KEY, bodyPartTraining);
                        startActivity(i);
                    });
                }

            }
        }
    }

    private void initViews() {
        trainingsRecView = findViewById(R.id.trainingsRecView);
        title = findViewById(R.id.title);
        duration = findViewById(R.id.duration);
        bodyPartName = findViewById(R.id.bodyPartName);
        nbTrainings = findViewById(R.id.nbTrainings);
        btnStart = findViewById(R.id.btnStart);
    }
}