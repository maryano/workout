package mg.itu.workout;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BodyPartTrainingItem implements Parcelable {

    private String name;
    private int duration;
    private int imgResourceId;
    private int bodyPartId;

    public BodyPartTrainingItem(String name, int imgResourceId, int bodyPartId) {
        this.name = name;
        this.imgResourceId = imgResourceId;
        this.bodyPartId = bodyPartId;
    }

    protected BodyPartTrainingItem(Parcel in) {
        name = in.readString();
        duration = in.readInt();
        imgResourceId = in.readInt();
        bodyPartId = in.readInt();
    }

    public static final Creator<BodyPartTrainingItem> CREATOR = new Creator<BodyPartTrainingItem>() {
        @Override
        public BodyPartTrainingItem createFromParcel(Parcel in) {
            return new BodyPartTrainingItem(in);
        }

        @Override
        public BodyPartTrainingItem[] newArray(int size) {
            return new BodyPartTrainingItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public int getImgResourceId() {
        return imgResourceId;
    }

    public int getDurationMin() {
        return (duration / 60)+1;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getBodyPartId() {
        return bodyPartId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(duration);
        parcel.writeInt(imgResourceId);
        parcel.writeInt(bodyPartId);
    }

}
