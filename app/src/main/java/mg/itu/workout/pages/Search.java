package mg.itu.workout.pages;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import mg.itu.workout.R;
import mg.itu.workout.Training;
import mg.itu.workout.Utils;
import mg.itu.workout.adapter.BodyPartTrainingAdapter;
import mg.itu.workout.adapter.TrainingAdapter;
import mg.itu.workout.adapter.TrainingAdapterSearch;

public class Search extends Fragment {

    private RecyclerView searchResult;
    private SearchView editSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        initViews(view);
        TrainingAdapterSearch adapter = new TrainingAdapterSearch(getActivity());
        searchResult.setAdapter(adapter);
        searchResult.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ArrayList<Training> trainings = Utils.getAllTrainings(getActivity());
        if (trainings != null) {
            adapter.setTrainings(trainings);
        }
        editSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                ArrayList<Training> trainings = Utils.searchTraining(s,getActivity());
                if(trainings != null){
                    adapter.setTrainings(trainings);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d("activity","string ato amn activity : "+s);
                ArrayList<Training> trainings = Utils.searchTraining(s,getActivity());
                if(trainings != null){
                    adapter.setTrainings(trainings);
                }
                return false;
            }
        });


        return view;

    }

    private void initViews(View view) {
        searchResult = view.findViewById(R.id.searchResult);
        editSearch = view.findViewById(R.id.searchView);
    }
}